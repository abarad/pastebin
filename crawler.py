import time
from urllib.parse import urljoin

import arrow
import requests
from bs4 import BeautifulSoup

from database import DBManager
from logger import logger
from paste import Paste


class PastebinCrawler:
    ARCHIVE_URL = 'https://pastebin.com/archive'
    logger = logger

    def __init__(self, connection_string: str):
        self.db = DBManager(connection_string)

    def _parse_html(self, url):
        self.logger.info(f'Parsing "{url}".')
        code = requests.get(url)
        plain = code.text
        return BeautifulSoup(plain, "html.parser")

    def _parse_paste(self, url):
        soup = self._parse_html(url)
        author = soup.find('div', {'class': 'username'}).a.contents[0]
        title = soup.find('div', {'class': 'info-top'}).h1.contents[0]
        content = soup.find('textarea', {'class': 'textarea'}).contents[0]
        date = arrow.get(soup.find('div', {'class': 'date'}).span['title'], 'dddd Mt[h] of MMMM YYYY HH:mm:ss A').to('UTC').datetime
        return Paste(url=url, author=author, title=title, content=content, date=date)

    def crawl(self, sleeping_time=120):
        while True:
            soup = self._parse_html(self.ARCHIVE_URL)
            table = soup.find('table', {'class': 'maintable'})
            pastes_urls = [urljoin(self.ARCHIVE_URL, row.td.a['href']) for row in table.findAll('tr') if row.td]
            self.logger.info(f'Fetching crawled URLs.')
            crawled_urls = self.db.get_urls()
            self.logger.info('Parsing new pastes.')
            new_pastes = [self._parse_paste(paste_url) for paste_url in pastes_urls if paste_url not in crawled_urls]
            if new_pastes:
                self.logger.info(f'Saving new pastes. {[p.url for p in new_pastes]}')
                self.db.add_records(pastes=new_pastes)
            else:
                self.logger.info('No new pastes to save.')
            logger.info(f'Sleeping for {sleeping_time} seconds.')
            time.sleep(sleeping_time)
