from typing import List

from sqlalchemy import Column, String, DateTime, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from paste import Paste

base = declarative_base()


class PastesTable(base):
    __tablename__ = 'pastes_table'
    url = Column(String, nullable=False, primary_key=True)
    author = Column(String)
    title = Column(String)
    date = Column(DateTime, nullable=False)
    content = Column(String, nullable=False)


class DBManager:
    def __init__(self, connection_string):
        self.engine = create_engine(connection_string)
        base.metadata.create_all(self.engine)

    def add_records(self, pastes: List[Paste]):
        session = sessionmaker(self.engine)()
        for p in pastes:
            record = PastesTable(url=p.url, author=p.author, title=p.title, date=p.date, content=p.content)
            session.add(record)
        session.commit()

    def get_urls(self):
        session = sessionmaker(self.engine)()
        return [r.url for r in session.query(PastesTable.url)]
