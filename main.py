import os

from crawler import PastebinCrawler

DB_CONNECTION_STRING = os.environ.get('DB_CONNECTION_STRING')

if __name__ == '__main__':
    crawler = PastebinCrawler(connection_string=DB_CONNECTION_STRING)
    crawler.crawl()
