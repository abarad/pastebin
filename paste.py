from datetime import datetime


class Paste:
    none_str = ('untitled', 'a guest', 'unknown', 'anonymous')

    def __init__(self, url: str, author: str, title: str, date: datetime, content: str):
        self.author = '' if author.lower() in self.none_str else author
        self.title = '' if title.lower() in self.none_str else title
        self.date = date
        self.content = content.strip()
        self.url = url
