To run this app you should use an env variable `DB_CONNECTION_STRING` as a SQL connection string.

You can create a dev database in [here](https://customer.elephantsql.com/)

